import datetime as dt
from marshmallow import Schema, fields

class Status():
    def __init__(self, description, amount, type):
        self.description = description
        self.amount = amount
        self.created_at = dt.datetime.now()
        self.type = type
        
    def __repr__(self):
        return '<Transaction(name={self.description!r})>'.format(self=self)
    
class StatusSchema(Schema): 
    description = fields.Str()
    amount = fields.Number()
    created_at = fields.Date()
    type = fields.Str()