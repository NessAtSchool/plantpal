import requests
from bs4 import BeautifulSoup
import support

class PlantScraper():
    def __init__(self):
        pass
    
    def get_url_for_plant(plant_name):
        base_url = "https://www.shootgardening.co.uk/plant/"
        specific_url = support.clean_string(plant_name)
        return base_url+specific_url
    
    def get_page_soup(url_string):
        page = requests.get(url_string)
        soup = BeautifulSoup(page.content, "html.parser")
        results = soup.find("div", class_="box box_selected")
        return results