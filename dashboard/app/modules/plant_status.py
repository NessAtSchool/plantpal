
class PlantStatus():
    def __init__(self, humidity, soil_moisture, lux, temp):
        self.humidity = humidity
        self.soil_moisture = soil_moisture
        self.lux = lux
        self.temp = temp

    def as_dict(self):
        status_dict = {
            'humidity' : self.humidity,
            'soil_moisture' : self.soil_moisture,
            'lux' : self.lux,
            'temp' : self.temp
        }
        
        return status_dict