from enum import Enum

class Status(Enum):
    LOW = 'LOW';
    GOOD = 'GOOD';
    HIGH = 'HIGH';