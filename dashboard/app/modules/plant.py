class Plant():
    def __init__(self, id, name, gender, personality, p_type, 
                 status=None, image=None, care_instructions=None, care_difficulty=None):
        self.id = id
        self.name = name
        self.gender = gender
        self.personality = personality
        self.p_type = p_type
        self.status = status
        self.image = image
        self.care_instructions = care_instructions
        self.care_difficulty = care_difficulty
        
    def print_plant(self):
        print("plant successfully created")
        print(self.id)
        print("given name: " + self.name)
        
    def as_dict(self):
        plant_dict = {
            'id' : self.id,
            'name' : self.name,
            'gender' : self.gender,
            'personality' : self.personality,
            'p_type' : self.p_type,
            'status' : self.status,
            'image' : self.image,
            'care_instructions' : self.care_instructions,
            'care_difficulty' : self.care_difficulty
        }
        
        return plant_dict
    
    def get_personality(self, string):
        self.personality = string

    def __str__(self):
        return f"{self.id} | name = {self.name}"
