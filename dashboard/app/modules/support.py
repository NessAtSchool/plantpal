
#general utilities
def clean_string(name_as_string):
        #take a string and make it into a parsable url format
        #that can be attached to the base url
        replace_dict = {'_': '-', ' ': '-', '+': '-', '*': '-'}
        lower_name_string = name_as_string.lower()
        #make a table with maketrans, turns dictionary into guideline for char replacement
        table = lower_name_string.maketrans(replace_dict)
        #translate acts as multiple str.replace() functions
        url_string = lower_name_string.translate(table)
        return url_string
    
def clean_string_for_internal_search(name_as_string):
    capitalised_string = name_as_string.capitalize()
    return capitalised_string