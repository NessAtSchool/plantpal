import json
from modules.plant_type import PlantType

with open("app/static/data/data.json", "r") as f:
    data = json.load(f)
    
print(data)

for key, value in data.items():
    print(key)
    print(value)
    
def expected_format(k, v):
    plant_name = k
    common_names = ""
    cultivation = ""
    soil_type = ""
    soil_drainage = ""
    soil_ph = ""
    light = ""
    placement_aspect = ""
    placement_exposure = ""
    image = ""
    
    if "Common names" in v:
        common_names = ", ".join(v["Common names"])
    if "Cultivation" in v:
        cultivation = v["Cultivation"]
    if "Soil" in v:
        soil_info = v["Soil"]
        if soil_info["Type"]:
            soil_type = soil_info["Type"]
        if soil_info["Drainage"]:
            soil_drainage = soil_info["Drainage"]
        if soil_info["pH"]:
            soil_ph = ", ".join(soil_info["pH"])
    if "Light" in v:
        light = v["Light"]
    if "Placement" in v:
        placement_info = v["Placement"]
        if "Aspect" in placement_info:
            placement_aspect = placement_info["Aspect"]
        if "Exposure" in placement_info:
            placement_exposure = placement_info["Exposure"]
    if "Image" in v:
        image = v["Image"]
        
        
    return {
        "plant_name": plant_name,
        "common_names": common_names,
        "cultivation": cultivation,
        "soil_type": soil_type,
        "soil_drainage": soil_drainage,
        "soil_ph": soil_ph,
        "light": light,
        "placement_aspect": placement_aspect,
        "placement_exposure": placement_exposure,
        "image": image
    }

print("\n\n\n")

formatted = []
for key, value in data.items():
    formatted.append(expected_format(key, value))
    
for f in formatted:
    print(f)

print("\nDONE")


for form in formatted:
    for res in form:
        print(f"<p>{res}: " +  "{{" + f"result.{res}" + "}}</p>")
    break