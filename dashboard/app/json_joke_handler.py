import json


def delete_data(joke_id):
    new_data = []
    with open('./app/static/data/jokes.json', 'r') as joke_list:
        data = json.load(joke_list)
    delete_option = joke_id - 1
    i = 0
    for entry in data:
        if i == int(delete_option):

            i += 1
        else:
            entry['Id'] = i + 1
            new_data.append(entry)
            i += 1

    x = 1

    for entry in new_data:
        entry['Id'] = x
        x += 1

    with open('./app/static/data/jokes.json', "w") as new_joke_list:
        json.dump(new_data, new_joke_list, indent=4)
    return


def reset_data():
    with open('./app/static/data/jokes.json', "r") as joke_list:
        jokes = json.load(joke_list)
    with open('./app/static/data/copy_jokes.json', "r") as copy_joke_list:
        copy_jokes = json.load(copy_joke_list)

        jokes = copy_jokes

    with open('./app/static/data/jokes.json', "w") as jsonFile:
        json.dump(jokes, jsonFile)
    return


def add_joke(new_joke):
    add_joke_json = {'Id': '', 'Text': '' + new_joke}
    with open('./app/static/data/jokes.json', "r") as joke_list:
        data = json.load(joke_list)
        data.append(add_joke_json)
        x = 1
        for entry in data:
            entry['Id'] = x
            x += 1
    with open('./app/static/data/jokes.json', "w") as jsonFile:
        json.dump(data, jsonFile)
    return
