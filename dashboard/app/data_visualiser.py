import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import os

#same for all graphs
day_labels  = ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT']
days = [1,2,3,4,5,6,7]
p_labels = ['Patricia', 'Randal', 'Senor']

#data for x axis
hydration = [[7, 6, 5, 5, 6, 7, 6],[8, 7, 7, 8, 7, 8, 8], [2, 2, 2, 2, 2, 2, 2]]
light = [[4, 4, 4, 5, 6, 4, 6],[8, 7, 7, 8, 7, 8, 8], [2, 2, 2, 2, 2, 2, 2]]
temp = [[6, 6, 6, 5, 6, 6, 6],[7, 7, 6, 5, 6, 7, 6], [2, 2, 2, 3, 2, 2, 2]]
happiness = [[7, 6, 5, 5, 6, 7, 6],[8, 7, 7, 8, 7, 8, 8], [2, 2, 2, 2, 2, 2, 2]]

colors = ['C0', 'C8', 'C4']

#define absolute path
graph_path = os.path.abspath("app/static/stats")
graph_file_hydration = 'weekly_hydration.png'
graph_file_light = 'weekly_light.png'
graph_file_temp = 'weekly_temperature.png'
graph_file_happiness = 'weekly_happiness.png'

def graph_draw(x, c, l, file_name):
    fig, ax = plt.subplots()
    for i in range(len(x)):
        ax.plot(days, x[i], color = c[i], marker='o', linestyle = 'solid', label = l[i])
    plt.ylim([1, 10])
    plt.xticks(days, day_labels)
    plt.legend()
    graph_save = os.path.join(graph_path, file_name)
    plt.savefig(graph_save, dpi = 300)

graph_draw(hydration, colors, p_labels, graph_file_hydration)
graph_draw(light, colors, p_labels, graph_file_light)
graph_draw(temp, colors, p_labels, graph_file_temp)
graph_draw(happiness, colors, p_labels, graph_file_happiness)