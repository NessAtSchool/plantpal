from app.or_objects import OpenRemotePlants
from app.or_asset_fetcher import AssetFetcher
from app.or_asset_updater import AssetUpdater

def test_updater_can_change_single_asset():
    id = OpenRemotePlants().plant_ids[0]
    updater = AssetUpdater(id)
    response = updater.change_asset_attribute("gender", "female")
    assert response.status_code == 200


def test_updater_and_fetcher_results_for_single_attribute():
    id = OpenRemotePlants().plant_ids[0]
    fetcher = AssetFetcher(id)
    updater = AssetUpdater(id)
    
    updater.change_asset_attribute("gender", "female")

    fetched_dict = fetcher.get_plant_as_dict()
    result_gender = fetched_dict["gender"]
    expected_gender = "female"
    assert result_gender == expected_gender

    updater.change_asset_attribute("gender", "male")
    fetched_dict = fetcher.get_plant_as_dict()
    result_gender = fetched_dict["gender"]
    expected_gender = "male"
    assert result_gender == expected_gender
    

def test_updater_success_on_kw_updates():
    id = OpenRemotePlants().plant_ids[0]
    fetcher = AssetFetcher(id)

    kw_updates = {
        "gender":"female",
        "personality":"sad"
    }
    updater = AssetUpdater(id, kw_updates)
    updater.update_attributes()

    fetched_dict = fetcher.get_plant_as_dict()
    result_gender = fetched_dict["gender"]
    expected_gender = "female"
    result_personality = fetched_dict["personality"]
    expected_personality = "sad"
    assert result_gender == expected_gender
    assert result_personality == expected_personality

    kw_updates = {
        "gender":"male",
        "personality":"angry"
    }
    updater = AssetUpdater(id, kw_updates)
    updater.update_attributes()

    fetched_dict = fetcher.get_plant_as_dict()
    result_gender = fetched_dict["gender"]
    expected_gender = "male"
    result_personality = fetched_dict["personality"]
    expected_personality = "angry"
    assert result_gender == expected_gender
    assert result_personality == expected_personality
