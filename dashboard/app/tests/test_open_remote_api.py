from app.or_objects import OpenRemotePlants, OpenRemoteAPI

def test_open_remote_api_asset_builds():
    plant_one = OpenRemotePlants().plant_ids[0]
    result = OpenRemoteAPI().asset_url(plant_one)
    expected = 'https://natlab.openremote.app/api/master/asset/1yXh6nFfRTwkMQmyBK0wjj'
    assert result == expected


def test_open_remote_api_asset_build_attribute():
    plant_one = OpenRemotePlants().plant_ids[0]
    result = OpenRemoteAPI().attribute_url(plant_one, 'personality')
    expected = 'https://natlab.openremote.app/api/master/asset/1yXh6nFfRTwkMQmyBK0wjj/attribute/personality'
    assert result == expected


def test_open_remote_api_builds_header():
    or_api = OpenRemoteAPI()
    or_api.client.token = "fake_token"
    result_header = or_api.headers()
    expected_header = {
        'accept': 'application/json',
        'Authorization': f'Bearer fake_token'
    }
    assert result_header == expected_header
