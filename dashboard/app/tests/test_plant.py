from app.modules.plant import Plant 

def test_create_plant():
    p = Plant("id", "name", "gender", "personality", "p_type")
    expected_dict = {
        'id' : "id",
        'name' : "name",
        'gender' : "gender",
        'personality' : "personality",
        'p_type' : "p_type"
    }

    result_dict = p.as_dict()
    assert expected_dict == result_dict


def test_plant_to_string():
    p = Plant("id_0", "name_0", "gender", "personality", "p_type")
    expected_string = "id_0 | name = name_0"
    result_string = str(p)
    assert expected_string == result_string