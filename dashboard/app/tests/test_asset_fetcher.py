from app.or_objects import OpenRemotePlants
from app.or_asset_fetcher import AssetFetcher
from app.modules.plant import Plant

def test_assert_returns_200_successfully():
    id = OpenRemotePlants().plant_ids[0]
    fetcher = AssetFetcher(id)
    response = fetcher.request_asset()
    assert response.status_code == 200

def test_creates_plant_object_from_response():
    id = OpenRemotePlants().plant_ids[0]
    fetcher = AssetFetcher(id)
    asset = fetcher.asset_as_object()
    assert type(asset) is Plant

def test_creates_dict_from_response():
    id = OpenRemotePlants().plant_ids[0]
    fetcher = AssetFetcher(id)
    asset = fetcher.get_plant_as_dict()
    assert type(asset) is dict
