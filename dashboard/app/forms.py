from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField, SelectField, TextAreaField
from wtforms.validators import DataRequired
import json


class LoginForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    remember_me = BooleanField('Remember Me')
    submit = SubmitField('Sign In')


class PlantSearchForm(FlaskForm):
    search = StringField('Plant name to search')
    submit = SubmitField('Search')


class ConfigurationForm(FlaskForm):
    name = StringField('Plant Name', validators=[DataRequired()])
    personality_choices = ['normal', 'sad', 'mad', 'happy']
    personality_choice = SelectField('Personality', choices= personality_choices, validators=[DataRequired()])
    genders = ["male", "female"]
    genders = SelectField('Gender', choices= genders, validators=[DataRequired()])
    sensors = ["6D9mvxZSkm9hw8PzdBVLsc", "4UojZm3stV2EjZjmPcHolv"]
    sensor_choice = SelectField('Sensor', choices=sensors, validators=[DataRequired()])
    # plant_type = StringField("Plant Type", render_kw={'readonly': True})
    #old plant type code, new can be a string
    #render kw refers tp the kw_updater from assetUpdater
    plant_type = StringField('Plant Type', validators=[DataRequired()])
    submit = SubmitField('Configure')
    
class AddPlantForm(FlaskForm): 
    name = StringField('Plant Name', validators=[DataRequired()])
    personality_choices = ['normal', 'sad', 'mad', 'happy']
    personality_choice = SelectField('Personality', choices = personality_choices, validators=[DataRequired()])
    genders = ['male', 'female']
    gender_choice = SelectField('Gender', choices = genders, validators=[DataRequired()])
    sensors = ['a1', 'b2']
    sensor_choice = SelectField('Sensor', choices= sensors, validators=[DataRequired()])
    plant_choices = ['a1', 'b2', 'c3']
    plant_type = SelectField('Plant Type', choices= plant_choices, validators=[DataRequired()])
    submit = SubmitField('Add Plant')
    
class DeleteJokeForm(FlaskForm):
    Id = StringField('Id', validators=[DataRequired()])
    text = StringField('Text', validators=[DataRequired()])


class AddJokeForm(FlaskForm):
    text = StringField('Text', validators=[DataRequired()])
    submit = SubmitField('Add')