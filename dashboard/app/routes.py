from urllib import response

import pycurl
import requests
from bson import ObjectId
from flask import render_template, flash, redirect, url_for
from app import app
from app import json_joke_handler
from app.forms import LoginForm, PlantSearchForm, ConfigurationForm, DeleteJokeForm, AddJokeForm, AddPlantForm
from app.model import Search, Plants, PlantUpdater
import json
import bson


@app.route('/')
@app.route('/my_plants', methods=['GET', 'POST'])
def my_plants():
    return render_template('my_plants.html', title='Your Plant Dashboard')  # add the form variables here


@app.route('/connected-plants', methods=['GET', 'POST'])
def get_connected_plants():
    plants = Plants().prepare_connected_plants()
    return render_template('connected_plants.html', title='Your Connected Plants', results=plants)


@app.route('/configureplant', methods=['GET', 'POST'])
@app.route('/configureplant/<plant_index>', methods=['GET', 'POST'])
def configureplant(plant_index=None):
    form = ConfigurationForm()
    plants = Plants()
    plant_index = int(plant_index)
    if form.is_submitted():
        try:
            plant = plants.get_plant_by_index(plant_index)
            updater = PlantUpdater(plant)
            updater.update_open_remote(form)
        except IndexError:
            flash(f"No plant at index {plant_index} found")
        return redirect(url_for('get_connected_plants'))
    form = plants.prepare_configure_form(plant_index, form)
    form.process()

    return render_template('configureplant.html', form=form)


@app.route('/moreinfo', methods=['GET', 'POST'])
@app.route('/moreinfo/<plant_index>', methods=['GET', 'POST'])
def moreinfo(plant_index=None):
    plants = Plants()
    plant_index = int(plant_index)
    plant = plants.get_plant_by_index(plant_index)

    return render_template('moreinfo.html', result=plant)


@app.route('/search', methods=['GET', 'POST'])
def search():
    form = PlantSearchForm()
    if form.validate_on_submit():
        flash('Searching for plant: {}'.format(
            form.search.data))
        return redirect(url_for("results", search_string=form.search.data))
    return render_template('search.html', form=form)


@app.route('/results/<search_string>')
def results(search_string):
    search_model = Search()
    search_result = search_model.search_algorithm(search_string)
    formatted_search_result = search_model.format_result_json(search_result)
    return render_template('results.html', results=formatted_search_result)


@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        flash('Login requested for user {}, remember_me={}'.format(
            form.username.data, form.remember_me.data))
        return redirect(url_for('index'))
    return render_template('login.html', title='Sign In', form=form)


@app.route('/add-plant', methods=['GET', 'POST'])
def add_plant():
    form = AddPlantForm()
    if form.validate_on_submit():
        return redirect(url_for('get_connected_plants'))
    return render_template('add-plant.html', form=form)


@app.route('/jokes', methods=['GET'])
def jokes():
    joke_response = requests.get('http://127.0.0.1:5000/get_all_jokes')
    json_joke_list = json.loads(joke_response.content)
    joke_list = json.loads(json_joke_list)
    for entry in joke_list:
        entry['_id'] = entry['_id']['$oid']
    print(joke_list)
    return render_template('jokes.html', results=joke_list)


@app.route('/jokes/delete', methods=['GET', 'POST'])
@app.route('/jokes/delete/<joke_id>', methods=['GET', 'POST'])
def jokes_delete(joke_id=None):
    form = DeleteJokeForm()
    if form.is_submitted():
        requests.post('http://127.0.0.1:5000/delete_joke', data=None, json={"_id": {"$oid": joke_id}})
        return redirect(url_for('jokes'))


@app.route('/jokes/reset', methods=['GET', 'POST'])
def jokes_reset():
    json_joke_handler.reset_data()
    return redirect(url_for('jokes'))


@app.route('/jokes/add', methods=['GET', 'POST'])
def joke_add():
    form = AddJokeForm()
    if form.is_submitted():
        flash('Joke test={}'.format(
            form.text.data))
        requests.post('http://127.0.0.1:5000/add_joke', data=None, json={"text": form.text.data})
        return redirect(url_for("jokes"))
    return render_template('add_jokes.html', form=form)
