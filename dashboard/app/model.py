import os
import json
from app import app
import random

from app.or_objects import OpenRemotePlants
from app.or_asset_fetcher import AssetFetcher
from app.or_asset_updater import AssetUpdater
from app.modules.sections import interested_keys



class Search():
    def __init__(self):
        pass
    

    def format_result_json(self, results):
        def expected_format(k, v):
            plant_name = k
            common_names = ""
            cultivation = ""
            soil_type = ""
            soil_drainage = ""
            soil_ph = ""
            light = ""
            placement_aspect = ""
            placement_exposure = ""
            image = ""
            
            if "Common names" in v:
                common_names = ", ".join(v["Common names"])
            if "Cultivation" in v:
                cultivation = v["Cultivation"]
            if "Soil" in v:
                soil_info = v["Soil"]
                if soil_info["Type"]:
                    soil_type = soil_info["Type"]
                if soil_info["Drainage"]:
                    soil_drainage = soil_info["Drainage"]
                if soil_info["pH"]:
                    soil_ph = ", ".join(soil_info["pH"])
            if "Light" in v:
                light = v["Light"]
            if "Placement" in v:
                placement_info = v["Placement"]
                if "Aspect" in placement_info:
                    placement_aspect = placement_info["Aspect"]
                if "Exposure" in placement_info:
                    placement_exposure = placement_info["Exposure"]
            if "Image" in v:
                image = v["Image"]
                
            care_levels = ['Easy', 'Medium', 'Difficult']
            care_level = random.choice(care_levels)
                
                
            return {
                "plant_name": plant_name,
                "common_names": common_names,
                "cultivation": cultivation,
                "soil_type": soil_type,
                "soil_drainage": soil_drainage,
                "soil_ph": soil_ph,
                "light": light,
                "placement_aspect": placement_aspect,
                "placement_exposure": placement_exposure,
                "image": image,
                "care_level": care_level
            }

        formatted = []
        for k, v in results.items():    
            formatted.append(
                expected_format(k, v)
            )
        return formatted

    def search_algorithm(self, search):
        result_keys = []
        
        json = self.read_plant_json()
        
        for entry in json:
            common_names = json[entry]["Common names"]
            for common_name in common_names:
                # find returns -1 if substring is not found
                lil_name = common_name.lower()
                lil_search = search.lower()
                if lil_name.lower().find(lil_search) > -1:
                    result_keys.append(entry)
                    
        results = {}
        for key in result_keys:
            results[key] = json[key]
        return results

    def read_plant_json(self):
        filename = os.path.join(app.static_folder, 'data', 'data.json')
        with open(filename) as test_file:
            data = json.load(test_file)
        return data
    
    def get_standard_sections(self):
        return interested_keys
        

class Plants():
    def __init__(self, plants=None):
        if plants is None:
            self.plant_ids = OpenRemotePlants().plant_ids
            self.plants = self.prepare_connected_plants()
        else:
            self.plants = plants

    def prepare_connected_plants(self):
        plants = []
        for index, asset_id in enumerate(self.plant_ids):
            fetcher = AssetFetcher(asset_id)
            plant_dict = fetcher.get_plant_as_dict()
            plant_dict["index"] = index
            plants.append(plant_dict)
        return plants

    def is_not_valid_index(self, index):
        return index > len(self.plants)    

    def prepare_configure_form(self, index, form):
        plant = self.plants[index]
        form.name.default = plant["name"]
        form.genders.default = plant["gender"]
        form.personality_choice.default = plant["personality"]
        form.plant_type.default = plant["p_type"]
        return form

    def get_plant_by_index(self, index):
        return self.plants[index]

class PlantUpdater:
    def __init__(self, plant):
        self.plant = plant

    def update_open_remote(self, form):
        name = form.name.data
        kw_updater = {
            "name" : name,
            "gender":form.genders.data,
            "personality":form.personality_choice.data,
            "sensor": form.sensor_choice.data,
            "plant_type": form.plant_type.data
        }
        asset_id = self.plant["id"]
        updater = AssetUpdater(asset_id, kw_updater)
        updater.update_attributes()
