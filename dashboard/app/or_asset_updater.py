import requests as rq
from app.or_objects import OpenRemoteAPI

class AssetUpdater():
    def __init__(self, asset_id, kw_updates={}):
        self.asset_id = asset_id
        self.kw_updates = kw_updates

    def update_attributes(self):
        for attribute, new_value in self.kw_updates.items():
            self.change_asset_attribute(attribute, new_value)

    def change_asset_attribute(self, attribute, value):
        or_api = OpenRemoteAPI()
        headers = or_api.headers()
        attribute_url = or_api.attribute_url(self.asset_id, attribute)
        json_data = value
        response = rq.put(attribute_url, headers=headers, json=json_data)
        return response
