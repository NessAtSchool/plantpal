import requests as rq
import json

from app.modules.plant import Plant
from app.modules.plant_status import PlantStatus
from app.or_objects import OpenRemoteAPI

class AssetFetcher():
    def __init__(self, asset_id):
        self.asset_id = asset_id

    def request_asset(self):
        or_api = OpenRemoteAPI()
        headers = or_api.headers()
        asset_url = or_api.asset_url(self.asset_id)
        response = rq.get(asset_url, headers=headers)
        return response

    def asset_as_object(self):
        response = self.request_asset()
        plant_json = json.loads(response.content.decode('utf-8'))
    
        id = plant_json['id']
        name = plant_json['attributes']['name']['value']
        attributes = plant_json['attributes']
        personality = attributes['personality']['value']
        gender = attributes['gender']['value']
        
        #status values 
        status_temp = attributes['status_temp']['value']
        status_humid = attributes['status_humid']['value']
        status_light = attributes['status_light']['value']
        status_moist = attributes['status_moist']['value']
        
        #specific characteristics
        plant_type = attributes['plant_type']['value']
        image = attributes['image']['value']
        care_instructions = attributes['notes']['value']
        care_difficulty = attributes['difficulty']['value']
        
        status = PlantStatus(status_humid, status_moist, status_light, status_temp)
        plant = Plant(id, name, gender, personality, plant_type, status, image, care_instructions, care_difficulty)
        
        return plant

    # decode response, parse it and extract data of interest from openremote attributes
    # this dictionary gets fed to the moreinfo route to be displayed
    def get_plant_as_dict(self):
        response = self.request_asset()
        plant_json = json.loads(response.content.decode('utf-8'))
        
        id = plant_json['id']
        name = plant_json['attributes']['name']['value']
        personality = plant_json['attributes']['personality']['value']
        gender = plant_json['attributes']['gender']['value']
        plant_type = plant_json['attributes']['plant_type']['value']
        image = plant_json['attributes']['image']['value']
        care_instructions = plant_json['attributes']['notes']['value']
        care_difficulty = plant_json['attributes']['difficulty']['value']
        
        #status preview 
        status_moist = plant_json['attributes']['status_moist']['value']
        status_light = plant_json['attributes']['status_temp']['value']
        status_humid = plant_json['attributes']['status_humid']['value']
        
        return {
            'id' : id,
            'name' : name,
            'gender' : gender,
            'personality' : personality,
            'p_type' : plant_type,
            'image' : image,
            'status_moist' : status_moist,
            'status_light' : status_light,
            'status_humid' : status_humid,
            'care_instructions' : care_instructions,
            'care_difficulty' : care_difficulty
        }