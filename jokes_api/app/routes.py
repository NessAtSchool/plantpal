import json

from bson import ObjectId, json_util
from flask import render_template, request, Response

from app import app
from app import plant_joke_scraper
from app.db_setup import jokesDb


@app.route('/')
@app.route('/index')
def index():
    return 'behold dog'

@app.route('/get_all_jokes')
def get_all_jokes():
    # need to do something with api key

    jokes_cursor = jokesDb.find()
    jokes_list = list(jokes_cursor)
    json_jokes = json.dumps(json_util.dumps(jokes_list))

    return json_jokes


@app.route('/import_jokes')
def import_jokes():
    joke_list = plant_joke_scraper.get_jokes()
    # I want to make a check here that check the amount of jokes in DB and the amount scraped
    try:
        for joke in joke_list:
            # print({"text": joke.text})
            jokesDb.insert_one({"text": joke.text})

        return '200'
    except:
        return '500'


@app.route('/add_joke', methods=['POST'])
def add_joke():
    # joke is json data. This must be added to the database
    content = request.json
    try:
        jokesDb.insert_one({"text": content['text']})
        return content['text']
    except:
        return '500'


@app.route('/update_joke', methods=['POST'])
def update_joke():
    content = request.json
    try:
        jokesDb.update_one({"_id": ObjectId(content['_id']['$oid'])}, {"$set": {"text": content['text']}})
        return '200'
    except:
        return '500'


@app.route('/delete_joke', methods=['POST'])
def delete_joke():
    # Joke_id is an int. This int needs to be used in a query for the database
    content = request.json
    try:
        jokesDb.delete_one({"_id": ObjectId(content['_id']['$oid'])})
        return '200'
    except:
        return '500'
