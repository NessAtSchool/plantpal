import requests
from bs4 import BeautifulSoup
from .models.joke import joke as Joke

URL = "https://www.boredpanda.com/plant-puns/?utm_source=google&utm_medium=organic&utm_campaign=organic"
page = requests.get(URL)

soup = BeautifulSoup(page.content, "html.parser")

jokes = soup.find_all("div", class_="open-list-item open-list-block clearfix")

formattedJokes = []


def get_jokes():
    for joke in jokes:
        id = (joke.find("div", class_="open-list-header")).text.replace('#', "")
        joke_text = (joke.find("p", class_="post-content-description text-open-list")).text
        joke = Joke(id, joke_text)
        formattedJokes.append(joke)


    return formattedJokes
