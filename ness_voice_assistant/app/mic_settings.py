RESPEAKER_RATE = 44100                # Sample rate of the mic.
RESPEAKER_CHANNELS = 1                  # Number of channel of the input device.
RESPEAKER_WIDTH = 2
RESPEAKER_INDEX = 0                     # run the check_device_id.py to get the mic index.
CHUNK = 1024                            # Number of frames per buffer.
WAVE_OUTPUT_FILEPATH = "audio/"  # Directory location ocation of all the output files.