import json
import logging
import random
import os.path
from os import path
from turtle import st
from urllib import response
from app.speech import speak
from app import plant_joke_scraper as pjs
import app.ai_chatbot_model as acm
import numpy


logger = logging.getLogger('voice assistant')



def process_text(text, pa):
   results = acm.model.predict([acm.bag_of_words(text,acm.words)])
   results_index = numpy.argmax(results)
   tag = acm.labels[results_index]
   
   #debugging
   print(tag)
   print(results)
   print(results_index)

   
   get_response(tag, numpy.max(results))

   return status   

def get_response(tag, accuracy):
   global status
   #default accuracy threshold
   dat = 0.75
 
   if tag == "greetings" and accuracy >= dat:
      select_response_from_tag(tag)
      status = "done"
   elif tag == "natlab_find_room" and accuracy >= dat:
      select_response_from_tag(tag)
      status = "done"
   elif tag =="tell_joke" and accuracy >= dat:
         joke_response()
         status = "done"
   elif tag == "goodbyes" and accuracy >= 0.6:
         select_response_from_tag(tag)
         status =  "goodbye"
   else:
      instruction_unkown_response()
      status = "done"

   return status   


def instruction_unkown_response():
   speak("I'm not sure if i understand")
   


def joke_response():
   if path.exists("repositories.json") == True:
      joke_list = pjs.get_jokes()
   else:
      pjs.get_new_jokes()
      joke_list = pjs.get_jokes()

   speak(select_joke_from_jokelist(joke_list))
   


def select_response_from_tag(tag):
   for tg in acm.data["intents"]:
      if tg["tag"] == tag:
         responses = tg["responses"]
   speak(random.choice(responses))      


def select_joke_from_jokelist(jokelist):
   selected_joke = random.choice(jokelist)
   return clean_string(selected_joke['joke_text'])

def clean_string(dirty_string):
   replacements = [
      ('/u2019', "'"),
      ('/201d', " ")
   ]

   for old, new in replacements:
      cleaned_string = dirty_string.replace(old, new)
      return cleaned_string
   
   
 




