import speech_recognition as sr
import pyaudio
import time
import wave
import threading
import os
from app import speech
from app import responses
import glob
import logging
import mic_settings as mc 


r = sr.Recognizer()
recognized_text = ''                    # Global variable for storing  audio converted text

class voice:
   
    def __init__(self):
        self.p = pyaudio.PyAudio()
        self.stream = self.p.open(
            rate=mc.RESPEAKER_RATE,
            format=pyaudio.paInt16,
            input_device_index=mc.RESPEAKER_INDEX,
            channels=mc.RESPEAKER_CHANNELS,
            input=True,
            frames_per_buffer=mc.CHUNK)

  
    def process(self, RECORD_SECONDS):
        frames = []
        for i in range(0, int(mc.RESPEAKER_RATE / mc.CHUNK * RECORD_SECONDS)):
            data = self.stream.read(mc.CHUNK, exception_on_overflow=False)
            frames.append(data)

        out_filename = mc.WAVE_OUTPUT_FILEPATH + str(time.time()) + ".wav"
        wf = wave.open(out_filename, 'wb')
        wf.setnchannels(mc.RESPEAKER_CHANNELS)
        wf.setsampwidth(self.p.get_sample_size(self.p.get_format_from_width(mc.RESPEAKER_WIDTH)))
        wf.setframerate(mc.RESPEAKER_RATE)
        wf.writeframes(b''.join(frames))
        wf.close()
        return out_filename

 
    def voice_command_processor(self, filename):
        global recognized_text
        
        with sr.AudioFile(filename) as source:
            r.adjust_for_ambient_noise(source=source, duration=0.5)
            wait_time = 3
            while True:
                audio = r.record(source, duration=3)
                if audio:
                    break
                time.sleep(0.1)
                wait_time = wait_time - 1
                if wait_time == 0:
                    break

            try:
                recognized_text = r.recognize_google(audio)
            except sr.UnknownValueError as e:
                pass
            except sr.RequestError as e:
                logger.error("service is down")
                pass
           
        os.remove(filename)
        return recognized_text   



a = voice()    # Initializing the voice class.


def set_logger():
    logger = logging.getLogger('voice assistant')
    logger.setLevel(logging.DEBUG)
    fh = logging.FileHandler("audio/voice.log")
    fh.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    fh.setFormatter(formatter)
    logger.addHandler(fh)

    return logger

def check_audio_device_id():
    p = pyaudio.PyAudio()
    info = p.get_host_api_info_by_index(0)
    numdevices = info.get('deviceCount')
    for i in range(0, numdevices):
        if (p.get_device_info_by_host_api_device_index(0, i).get('maxInputChannels')) > 0:
            print("Input Device id: "+ str(i)+ " - "+str(p.get_device_info_by_host_api_device_index(0, i).get('name')))

def capture_user_speech(duration):
    command_file_name = a.process(duration)
    a.voice_command_processor(command_file_name)

def listen_for_commands(duration):
    capture_user_speech(duration)
    if recognized_text != '':
        return
    else:
        speech.speak("I didn't catch that, mind saying it again?")
        capture_user_speech(duration)   
     

def remove_user_audio_files():
    files = glob.glob(os.path.join(mc.WAVE_OUTPUT_FILEPATH + '*.wav'))
    for file in files:
        os.remove(file)
    

if __name__ == 'app.main':

    #Debugging
    check_audio_device_id()
    logger = set_logger()
    

    while True:
        global status
        file_name = a.process(3)
        logger.info("wake_word said :: " + recognized_text)
        #print("wake_word said :: " + recognized_text)
        if "hello" in recognized_text:
            logger.info("wake word detected...")
            recognized_text = ''
            status = "none"
            speech.speak("hey i'm plant bot, whats up?")
            time.sleep(0.3)

            print(status)            
            while status != 'goodbye':
                print(status)
                if status == 'done':
                    time.sleep(0.3)
                    speech.speak("Is there anything else I can do for you? If not say bye!")
                    recognized_text = ''
                     
                listen_for_commands(5)
                logger.info("you said :: " + recognized_text)
                status = responses.process_text(recognized_text, a)  

            remove_user_audio_files()    
            recognized_text = ''
            status = "none"        

        else:
            t1 = threading.Thread(target=a.voice_command_processor, args=(file_name,))
            t1.start()
