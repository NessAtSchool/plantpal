import requests
from bs4 import BeautifulSoup
import  json
from section import interested_keys

def get_url_for_plant(plant_name):
    base_url = "https://www.shootgardening.co.uk/plant/"
    specific_url = clean_string(plant_name)
    return base_url+specific_url
    
    
def clean_string(name_as_string):
    #take a string and make it into a parsable url format
    #that can be attached to the base url
    replace_dict = {'_': '-', ' ': '-', '+': '-', '*': '-'}
    lower_name_string = name_as_string.lower()
    #make a table with maketrans, turns dictionary into guideline for char replacement
    table = lower_name_string.maketrans(replace_dict)
    #translate acts as multiple str.replace() functions
    url_string = lower_name_string.translate(table)
    return url_string

# temporary string cleaning for internal json search
def clean_string_for_internal_search(name_as_string):
    capitalised_string = name_as_string.capitalize()
    return capitalised_string


def get_page_soup(url_string):
    # takes in raw url string and 
    # soupifies it and 
    # spits back out the raw page data
    page = requests.get(url_string)
    soup = BeautifulSoup(page.content, "html.parser")
    results = soup.find("div", class_="box box_selected")
    return results

def process_soup_by_tag(results):
    plant_data = {}
    
    all_h3 = results.find_all("h3")
    for h3 in all_h3:
        h3_text = h3.text
        p_soup = h3.find_next("p")
        
        if p_soup.find("span", "mobile-links"):
            p = p_soup.a.text
        else:
            p = p_soup.text.strip()
        plant_data[h3_text] = p
    return plant_data


def process_interested_keys(plant_data):
    cleaned_plant_data = {}
    for k in interested_keys:
        plant_key = clean_string(k)
        value = plant_data[k]
        cleaned_plant_data[plant_key] = value
    return cleaned_plant_data

# handles finding plant search
# as soon as the plant is found the iterator terminates
def find_plant(search_string, data):
    try:
        plant_searched = next(
            x for x in data if x['botanical-name'] == search_string
        )
    except:
        plant_searched = ''
    finally:
        return plant_searched

def get_plant_key_by_value(search_name, data_structure):
    searched_name = []
    for key, val in data_structure.items():
        if search_name in val:
            searched_name.append(key)
    
    return str(searched_name)
# write to/ read from json file    

def write_json(file_string, data_structure):
    with open(file_string, 'w') as outfile: 
        json.dump(data_structure, outfile)
        
def load_json(file_string):
    json_as_list = []
    with open(file_string, "r") as f: 
        json_as_list = json.load(f)
    
    return json_as_list

