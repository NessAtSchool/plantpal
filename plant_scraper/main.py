import scraper

plant_data_load = []
plant_names_load = {}
plant_data_load = scraper.load_json('data.json')
plant_names_load = scraper.load_json('names.json')

# for keys, values in plant_names_load.items():
#     print(keys)
#     print(values)

# # get user input and make it usable for the system
search_input = input("Please input plant name:")
formatted_search_input = scraper.clean_string_for_internal_search(search_input)

# print(formatted_search_input)

searched_plant = {}


# botanical_name_from_search = scraper.get_plant_key_by_value(formatted_search_input, plant_names_load)
# print('this is what I found: ' + botanical_name_from_search)
searched_plant = scraper.find_plant(formatted_search_input, plant_data_load)
print(len(plant_data_load))

# if the length of the returned dictionary is 0
    # i.e. no results were found
# scrape the data instead
if len(searched_plant) == 0:
    print(searched_plant)
    plant_name_clean = scraper.clean_string(formatted_search_input)
    plant_url = scraper.get_url_for_plant(plant_name_clean)
    plant_soup = scraper.get_page_soup(plant_url)
    
    all_plant_key_data = scraper.process_soup_by_tag(plant_soup)
    searched_plant = scraper.process_interested_keys(all_plant_key_data)
    plant_data_load.append(searched_plant)
    
    b_name = searched_plant['botanical-name']
    o_names = searched_plant['other-names']
    plant_names_load[b_name] = o_names
    
    scraper.write_json('data.json', plant_data_load)
    scraper.write_json('names.json', plant_names_load)
 

print(len(plant_data_load))
print(searched_plant)