import scraper

def test_get_url_for_plant_outputs_correct_string():
    name = 'acacia-pycnantha'
    response = scraper.get_url_for_plant(name)
    assert response == "https://www.shootgardening.co.uk/plant/acacia-pycnantha"
    
def test_get_page_soup_not_null():
    soup = scraper.get_page_soup("https://www.shootgardening.co.uk/plant/acacia-pycnantha")
    assert soup != None
    
def test_get_page_soup_null():
    ...