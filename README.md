# PlantPal

## Getting started
This repository contains the entirety of the Plant Pal project. Check the corresponding project wiki for information on starting up the project, testing and architecture.
For general design, implementation and research documents please check the Documentation folder in the repository. 

## Plant Pal - Happy Plants, Happy People
Plant Pal is a comprehensive IoT system which allows users to interact with their plants. The core of the Plant Pal system is OpenRemote, an open source IoT platform that simplifies the communication between Plant Pal's different elements. For more specific information on Plant Pal, please visit the wiki!

## Installation
Each component of Plant Pal runs separately at the moment - chapter 2 of the wiki details project structure, setup and file structure. 


## Roadmap
Chapter 3 of the wiki goes into known problems and to-do items leftover from the previous semesters. As well as suggestions for improvement. 



