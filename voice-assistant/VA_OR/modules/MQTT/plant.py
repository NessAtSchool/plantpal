class Plant:
  
  def __init__(self, name, id):
    self.name = name
    self.id = id
    self.temperature = "good"
    self.humidity = "good"
    self.moisture = "good"
    self.light = "good"
    self.personality = "general"
    self.gender = "male"
    self.type = "plant"
