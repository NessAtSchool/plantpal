import paho.mqtt.client as mqttClient
import time
import certifi
import json
from plant import Plant

frank = Plant("Frank", "1yXh6nFfRTwkMQmyBK0wjj")
patricia = Plant("Patricia", "7YH4V7Rj6eEtJqq2nkPkCH")

def on_connect(client, userdata, flags, rc):
  
    if rc == 0:
  
        print("Connected to broker")
  
        global Connected                #Use global variable
        Connected = True                #Signal connection 
  
    else:
  
        print("Connection failed")
  
def on_message(client, userdata, message):
    msg = json.loads(message.payload.decode("utf-8"))
    id = msg["attributeState"]["ref"]["id"]
    att_name = msg["attributeState"]["ref"]["name"]
    att_value = msg["attributeState"]["value"]
    if id == frank.id:
        set_value(frank, att_name, att_value)
    elif id == patricia.id:
        set_value(patricia, att_name, att_value)

def set_value(plant, attribute, value):
    if attribute == "status_temp":
        plant.temperature = value
    elif attribute == "status_humid":
        plant.humidity = value
    elif attribute == "status_moist":
        plant.moisture = value
    elif attribute == "status_light":
        plant.light = value
    elif attribute == "plant_type":
        plant.type = value
    elif attribute == "gender":
        plant.gender = value
    elif attribute == "personality":
        plant.personality = value
    print(plant.name + ": " + attribute + " changed to: " + value)
  
Connected = False   #global variable for the state of the connection
  
broker_address= "natlab.openremote.app" 
port = 8883                         
user = "master:userva"                   
password = "IhjANcl5KdVpU37eclrk1AgbxTaac90W"     
  
client = mqttClient.Client("VA_1")               
client.username_pw_set(user, password=password)   
client.tls_set(certifi.where())
client.on_connect= on_connect                    
client.on_message= on_message                     
  
client.connect(broker_address, port=port)        
  
client.loop_start()       
  
while Connected != True:  
    time.sleep(0.1)


client.subscribe("master/VA_1/attribute/+/1yXh6nFfRTwkMQmyBK0wjj")
client.subscribe("master/VA_1/attribute/+/7YH4V7Rj6eEtJqq2nkPkCH")
  
try:
    while True:
        time.sleep(1)
  
except KeyboardInterrupt:
    print("exiting")
    client.disconnect()
    client.loop_stop()