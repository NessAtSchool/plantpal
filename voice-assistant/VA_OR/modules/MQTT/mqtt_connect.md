
# Connecting to OpenRemote using the MQTT API

## Prerequisites

In order to subscribe to assets and attributes, using the OpenRemote MQTT Manager API, the following packages are used:  

- Paho-mqtt ( >= 1.6.1 )
- Certifi   ( >= 2022.5.18.1 )
- Python    ( >= 3.10.4 )
    - JSON      ( Python Standard Library )
    - Time      ( Python Standard Library )

To be able to authenticate you'll need to create a service user with read access and an asset using the Manager UI (must be logged in as super user to access this functionality), please refer to the OpenRemote [Manager UI user guide](https://github.com/openremote/openremote/wiki/User-Guide%3A-Manager-UI);

## Code Structure

> The full code can be found in the mqttOR.py file.

Before you can start receiving messages from OpenRemote, some settings need to be configured.

![Service user account settings](images/account.png)