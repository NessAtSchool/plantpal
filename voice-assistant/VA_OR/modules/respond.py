import json
import random

with open('responses_general.json', 'r') as file:
    data = json.load(file)
    jokes = data['jokes']
    greetings = data['greetings_general']
    responses_all = data['responses_all']
    responses_one = data['responses_one']
    responses_two = data['responses_two']
    responses_three = data['responses_three']

def getUpdateAll():
    return random.choice(tuple(responses_all))


def getUpdateOne(name):
    if name == 'one':
        return random.choice(tuple(responses_one))
    elif name == 'two':
        return random.choice(tuple(responses_two))
    elif name == 'three':
        return random.choice(tuple(responses_three))

def getGreeting():
    return random.choice(tuple(greetings))

def getJoke():
    return random.choice(tuple(jokes))