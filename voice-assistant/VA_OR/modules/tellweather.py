import requests

# Call on openweathermap for current weather data
def getWeather():
    api_key = "fe3f7685f6b738e38561dc48c41f3c22"
    base_url = "https://api.openweathermap.org/data/2.5/weather?"
    city = "Eindhoven, NL"
    complete_url = base_url+"appid="+api_key+"&q="+city
    response = requests.get(complete_url)
    x = response.json()
    if x["cod"] != "404":
        y = x["main"]
        current_temp = round(y["temp"]-273.15, 1)
        current_humidity = y["humidity"]
        z = x["weather"]
        description = z[0]["description"]
        return f"Current temperature is {current_temp} degrees celcius, humidity in percentage is {current_humidity}. {description}"
