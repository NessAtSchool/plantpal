from urllib import response
import speech_recognition as sr
import pyttsx3
import datetime
import time
from modules import respond
from modules import tellweather as we
from modules import telldate as da

engine = pyttsx3.init()
voices = engine.getProperty('voices')
engine.setProperty('voice', voices[0].id)

# Text to speech
def talk(text):
    engine.say(text)
    engine.runAndWait()
    print(text)

# Listen for command and transfer speech to text
def getCommand():
    text = sr.Recognizer()
    with sr.Microphone() as source:
        print("Listening...")
        text.adjust_for_ambient_noise(source, duration=0.2)
        audio = text.listen(source)

        try:
            statement = text.recognize_google(audio, language='en-en')
            print(f"Command: {statement}\n")

        except Exception as e:
            return "None"
        return statement

# Wakeup greeting
def greeting():
    current_hour = datetime.datetime.now().hour
    if current_hour >= 0 and current_hour < 12:
        talk("Good Morning!")
    elif current_hour >= 12 and current_hour < 18:
        talk("Good Afternoon!")
    else:
        talk("Good Evening!")

# Match the given command with preset keywords and respond
def matchCommand(text):
    if 'good bye' in text or 'ok bye' in text or 'sleep' in text:
        talk("I'm going to sleep, good bye")

    elif 'update' in text:
        if 'all' in text:
            answer = respond.getUpdateAll()
            talk(answer)
        elif '1' in text or 'one' in text or 'on' in text:
            answer = respond.getUpdateOne('one')
            talk(answer)
        elif '2' in text or 'two' in text or 'too' in text:
            answer = respond.getUpdateOne('two')
            talk(answer)
        elif '3' in text or 'three' in text or 'tree' in text:
            answer = respond.getUpdateOne('three')
            talk(answer)

    elif 'time' in text:
        timeStr = datetime.datetime.now().strftime("%H:%M:%S")
        talk(f"The current time is {timeStr}")

    elif 'date' in text:
        talk(da.getDate())

    elif 'weather' in text:
        talk(we.getWeather())

    elif 'who made you' in text or 'who created you' in text:
        talk("I was built by a Fontys student named Nina.")

    elif 'who are you' in text or 'what can you do' in text:
        talk("I am Tommy version 1 point 0, your personal plant assistant. I am programmed to perform minor tasks like tell time, predict weather, give plant status updates and tell jokes.")

    elif 'joke' in text or 'make me laugh' in text:
        talk(respond.getJoke())

    else:
        talk("I don't understand, can you repeat that?")

print('Tommy is waking up.')
talk("Waking up.")
greeting()

if __name__ == '__main__':

    while True:
        # Listen
        statement = getCommand().lower()
        if statement == 0:
            continue

        # Check for wakeup word
        if 'tommy' in statement:
            # Respond + Tone
            talk(respond.getGreeting())
            talk('beeep')

            # Get command
            statement = getCommand().lower()
            if statement == 0:
                continue

            # Give appropriate response
            matchCommand(statement)
            
time.sleep(3)