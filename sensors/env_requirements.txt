certifi==2022.5.18.1
paho-mqtt==1.6.1
setuptools==58.1.0
websocket_client==1.3.2
yapf==0.32.0