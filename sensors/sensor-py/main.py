import paho.mqtt.client as mqttClient
import time
import certifi
import socket
import json
from _thread import start_new_thread
from config import PLANT_ASSETS, BROKER_ADDRESS, PORT, USER, PASSWORD

Connected = False  #global variable for the state of the connection

def on_connect(client, userdata, flags, rc):
    if rc == 0:

        print("Connected to broker")

        global Connected  #Use global variable
        Connected = True  #Signal connection

    else:

        print("Connection failed")


def on_publish(client, userdata, result):
    print("Data published \n")
    pass



def mqtt_connect(payload, id):
    clientMQTT = mqttClient.Client(id)
    clientMQTT.username_pw_set(USER, password=PASSWORD)
    clientMQTT.tls_set(certifi.where())
    clientMQTT.on_connect = on_connect
    clientMQTT.on_publish = on_publish
    clientMQTT.connect(BROKER_ADDRESS, port=PORT)
    clientMQTT.loop_start()

    while Connected != True:
        time.sleep(0.1)

    # Publish payload to attributes
    for plant in PLANT_ASSETS:
        if plant == id:
            assetID = PLANT_ASSETS.get(plant)
            clientMQTT.publish(f"master/{id}/writeattributevalue/temperature/{assetID}", round(payload['temperature'], 2))
            clientMQTT.publish(f"master/{id}/writeattributevalue/humidity/{assetID}", round(payload['humidity'], 2))
            clientMQTT.publish(f"master/{id}/writeattributevalue/soil_moisture/{assetID}", round(payload['soil'], 2))
            clientMQTT.publish(f"master/{id}/writeattributevalue/lux/{assetID}", round(payload['lux'], 2))

    clientMQTT.disconnect()
    clientMQTT.loop_stop()
    

def on_new_client(clientsocket,addr):
    while True:
        msg = clientsocket.recv(1024)
        if len(msg) == 0:
            break

        else:
            data = msg.decode('utf8')
            try:
                dict_json = json.loads(data)
                print(dict_json)
                mqtt_connect(dict_json, addr[0])
            except json.JSONDecodeError as e:
                print("JSON:", e)
            
    print("Closing connections")  
    clientsocket.close()

host, port = '0.0.0.0', 8090
lsock = socket.socket()
lsock.bind((host, port))
lsock.listen(1)
print(f"Listening on {(host, port)}")

try:
    while True:
        c, addr = lsock.accept()     # Establish connection with client.
        start_new_thread(on_new_client, (c, addr))
except KeyboardInterrupt:
    print("Caught keyboard interrupt, exiting")
finally:
    lsock.close()