
#include <dummy.h>

#include <WiFi.h>
#include <DHT.h>
#include <BH1750.h>
#include <Wire.h>
#include <ArduinoJson.h>

const int led = 13;

#define I2C_SDA 25
#define I2C_SCL 26
#define DHT_PIN 16
#define BAT_ADC 33
#define SOIL_PIN 32
#define BOOT_PIN 0
#define POWER_CTRL 4
#define USER_BUTTON 35
#define DHT_TYPE DHT11
#define uS_TO_S_FACTOR 1000000ULL //Conversion factor for micro seconds to seconds
#define TIME_TO_SLEEP  10800 

DHT dht(DHT_PIN, DHT_TYPE);
BH1750 lightMeter(0x23);

const char* ssid = "POCOF3";
const char* password =  "marc12344";
 
const uint16_t port = 8090;
const char * host = "192.168.88.182";

// Set to true for Serial monitor messages and shorter sleep timer
const bool DEBUG = true;
 
void setup()
{
    Serial.begin(115200);

    WiFi.begin(ssid, password);
    int timeout_counter = 0;
    while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        if(DEBUG) { Serial.println("..."); }
        timeout_counter++;
        if(timeout_counter >= 50){
        ESP.restart();
        }
    }
 
    if(DEBUG) { Serial.print("WiFi connected with IP: "); }
    if(DEBUG) { Serial.println(WiFi.localIP()); }

    dht.begin();

    pinMode(POWER_CTRL, OUTPUT);
    digitalWrite(POWER_CTRL, 1);
    delay(1000);

    bool wireOk = Wire.begin(I2C_SDA, I2C_SCL);
    bool lightOk = lightMeter.begin(BH1750::CONTINUOUS_HIGH_RES_MODE);
    if(DEBUG) { 
      if (wireOk) {
        Serial.println(F("Wire ok"));
      } else {
        Serial.println(F("Wire NOK"));
      }
    
      if (lightOk) {
        Serial.println(F("BH1750 Advanced begin"));
      } else {
        Serial.println(F("Error initialising BH1750"));
      }
    }
    getLightValue(); 

    getTemperatureValue();
    
    getHumidityValue();

    getLightValue();

    getSoilValue();
}

void loop()
{
    WiFiClient client;

    StaticJsonDocument<100> doc;    
    JsonObject object = doc.to<JsonObject>();
 
    if (!client.connect(host, port)) {
      if(DEBUG) { Serial.println("Connection to host failed"); }
      delay(1000);
      return;
    }
 
    if(DEBUG) { Serial.println("Connected to server successful!"); }

    getLightValue(); 

    object["temperature"] = getTemperatureValue();
    
    object["humidity"] = getHumidityValue();

    object["lux"] = getLightValue();

    object["soil"] = getSoilValue();

    char output[100];
    serializeJson(doc, output);
    client.print(output);
    if(DEBUG) { Serial.println("Disconnecting..."); }
    client.stop();

    //WiFi.disconnect(true);
    //WiFi.mode(WIFI_OFF);
    //btStop();

    // Configure the timer to wake up 
    //esp_sleep_enable_timer_wakeup(10 * uS_TO_S_FACTOR);
    //esp_sleep_enable_timer_wakeup(TIME_TO_SLEEP * uS_TO_S_FACTOR);

    // Go to sleep! 
    //esp_deep_sleep_start();
}

float getTemperatureValue(){
    float temp = dht.readTemperature();
    if(DEBUG) { Serial.print("Temperature: "); Serial.println(temp); }
    delay(500);
    return temp;
}

float getHumidityValue(){
    float humidity = dht.readHumidity();
    if(DEBUG) { Serial.print("Humidity: "); Serial.println(humidity); }
    delay(500);
    return humidity;
}

float getLightValue(){
    if (lightMeter.measurementReady()) {
      float lux = lightMeter.readLightLevel();
      if(DEBUG) { Serial.print("Light: "); Serial.print(lux); Serial.println(" lx"); }
      delay(500);
      return lux;
    } 
    return 0;
}

float getSoilValue(){
    uint16_t soil = analogRead(32);
    if(DEBUG) { Serial.print("Soil before map: "); Serial.println(soil); }
    float soilNew = map(soil, 1616, 3550, 100, 0);
    if(DEBUG) { Serial.print("Soil: "); Serial.println(soilNew); }
    delay(500);
    return soilNew;
}
