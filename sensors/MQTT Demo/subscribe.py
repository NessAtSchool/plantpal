import paho.mqtt.client as mqttClient
import time
import certifi
import json

Connected = False  #global variable for the state of the connection
username = 'master:usermqtt'
secret = 'KaLJkMoDbgv4Vwuw2IKYn4wUTj9a15fB'
host = 'natlab.openremote.app'
port = 8883
clientID = 'MQTTsub'
assetID = '6oIoXiueHOisQXDXDp3NrJ'
attribute = 'subscribe'


def on_connect(client, userdata, flags, rc):
  
    if rc == 0:
  
        print("Connected to broker")
  
        global Connected                #Use global variable
        Connected = True                #Signal connection 
  
    else:
  
        print("Connection failed")


def on_message(client, userdata, message):
    msg = json.loads(message.payload.decode("utf-8"))
    id = msg["attributeState"]["ref"]["id"]
    att_name = msg["attributeState"]["ref"]["name"]
    att_value = msg["attributeState"]["value"]
    print(f'Message received. Asset ID: {id}. Attribute name: {att_name}. Attribute value: {att_value}.')
  
  
client = mqttClient.Client(clientID)               
client.username_pw_set(username, password=secret)   
client.tls_set(certifi.where())
client.on_connect= on_connect                    
client.on_message= on_message                     
client.connect(host, port=port)        
client.loop_start()       
  
while Connected != True:  
    time.sleep(0.1)


client.subscribe(f"master/{clientID}/attribute/{attribute}/{assetID}")
  
try:
    while True:
        time.sleep(1)
  
except KeyboardInterrupt:
    print("Exiting")
    client.disconnect()
    client.loop_stop()