import paho.mqtt.client as mqttClient
import time
import certifi

Connected = False  #global variable for the state of the connection
username = 'master:usermqtt'
secret = 'KaLJkMoDbgv4Vwuw2IKYn4wUTj9a15fB'
host = 'natlab.openremote.app'
port = 8883
clientID = 'MQTTpub'
assetID = '6oIoXiueHOisQXDXDp3NrJ'
attribute = 'publish'
attribute_value = 11

def on_connect(client, userdata, flags, rc):
    if rc == 0:

        print("Connected to broker")

        global Connected  #Use global variable
        Connected = True  #Signal connection

    else:

        print("Connection failed")


def on_publish(client, userdata, result):
    print("Data published \n")
    pass



clientMQTT = mqttClient.Client(clientID)
clientMQTT.username_pw_set(username, password=secret)
clientMQTT.tls_set(certifi.where())
clientMQTT.on_connect = on_connect
clientMQTT.on_publish = on_publish
clientMQTT.connect(host, port=port)
clientMQTT.loop_start()

while Connected != True:
    time.sleep(0.1)

# Publish payload to attributes

clientMQTT.publish(f"master/{clientID}/writeattributevalue/{attribute}/{assetID}", attribute_value)

clientMQTT.disconnect()
clientMQTT.loop_stop()